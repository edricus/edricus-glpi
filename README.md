# edricus-glpi
Automatise l'installation de serveurs et de clients GLPI.  
Utilise la dernière version de GLPI.  

## Utilisation
### Linux
```
Usage:	./glpi.sh [OPTIONS]
	-s, --server			Install GLPI server
	-c, --client			Install GLPI client
```
<h3> Windows </h3>
Activer l'exécution des scripts powershell sous Windows.  

Ce script installe uniquement l'agent GLPI

```
Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force
```
