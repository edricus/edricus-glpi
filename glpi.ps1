# Téléchargement de la dernière version de l'agent
$repo = "glpi-project/glpi-agent"
$file = "GLPI-Agent-1.4-x64.msi"
$releases = "https://api.github.com/repos/$repo/releases"
$tag = (Invoke-WebRequest $releases -UseBasicParsing | ConvertFrom-Json)[0].tag_name
$download = "https://github.com/$repo/releases/download/$tag/$file"
$name = $file.Split(".")[0]
$msi = "$name-$tag.msi"
$dir = "$name-$tag"
Invoke-WebRequest $download -Out $msi

# Installation de l'agent
# /silent ne fonctionne pas
$server = Read-Host -Prompt 'IP du serveur GLPI'
msiexec /i $msi SERVER='http://$server/glpi/fusioninventory/' ADDLOCAL=ALL DEBUG=2 ADD_FIREWALL_EXCEPTION=1 RUNNOW=1 TASK_FREQUENCY=daily
