#!/bin/bash
if [[ "$EUID" -ne 0 ]]; then 
  echo "Ce script s'exécute en root"
  exit
fi
install_server() {
glpi_dir=/var/www/html/glpi
# Dépendences
apt install \
	curl wget \
	php php-ldap php-imap php-apcu php-xmlrpc php-cas php-mysqli php-mbstring php-curl php-gd php-simplexml php-xml php-intl php-zip php-bz2 \
	apache2 mariadb-server perl -y
# Téléchrgement de GLPI
curl -s https://api.github.com/repos/glpi-project/glpi/releases/latest |\
	grep "browser_download_url.*tgz" |\
	cut -d : -f 2,3 |\
	tr -d \" |\
	wget --show-progress -qi -
tar xzf glpi*.tgz -C /var/www/html/
rm -rf glpi*.tgz
chown -R www-data:www-data /var/www/html/glpi
# Configuration de MYSQL
mysql_secure_installation
mysql << MYSQL
create database glpi;
create user 'glpiuser'@'localhost' identified by 'glpi';
GRANT ALL PRIVILEGES ON glpi.* TO 'glpiuser'@'localhost';
FLUSH PRIVILEGES;
MYSQL
# Installation de GLPI
console=/var/www/html/glpi/bin/console
$console db:install \
	--reconfigure \
	--db-name=glpi \
	--db-user=glpiuser \
	--db-password=glpi \
	--no-telemetry \
	--no-interaction
# Activation des services
systemctl enable --now apache2 mariadb
echo "Fin de l'installtion - serveur GLPI"
echo "URL du serveur : http://localhost/glpi"
echo "Identifiants de première connection :"
echo "	- Utilisateur: glpi"
echo "	- Mot de passe: glpi"
}

install_client() {
read -p "IP du serveur GLPI ? " SERVER_IP
SERVER_URL="http://$SERVER_IP/glpi/front/inventory.php"
apt-get update 
curl -s https://api.github.com/repos/glpi-project/glpi-agent/releases/latest |\
	grep -m1 "browser_download_url.*linux-installer.pl" |\
	cut -d : -f 2,3 |\
	tr -d \" |\
	wget --show-progress -qi -
chmod u+x *linux-installer.pl
INSTALLER=$(ls | grep installer.pl)
./$INSTALLER -s $SERVER_URL --runnow
echo "Fin de l'installation - client GLPI"
}
# Interaction
if [[ $1 == '-s' ]] || [[ $1 == '--server' ]]; then
	install_server
elif [[ $1 == '-c' ]] || [[ $1 == '--client' ]]; then
	install_client
else
	echo "Usage:	$0 [OPTIONS]"
	echo '	-s, --server			Install GLPI server'
	echo '	-c,	--client			Install GLPI client'
fi

